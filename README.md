# Описание

Docker-compose для запуска приложения Python+Postgres

## Запуск

```
git clone https://gitlab.com/apashin/dcompose-python-sample-app.git
cd dcompose-python-sample-app
docker-compose up -d

```

## Проверка

curl -i -X POST -d '{ "username": "user123", "email": "user@example.com", "password_hash": "example" }' -H "Content-Type: application/json" http://127.0.0.1:5000/api/user

